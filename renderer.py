#! /usr/bin/env python3

from mako.template import Template

def getPageList(website_file):
    page_list = []
    with open(website_file) as f:
        for line in f:
            line = line.rstrip('\n')
            website = line.split(':')
            page_list.append(website)

    return(page_list)



if __name__=='__main__':
    template_file = "index.html"
    website_file = "websitelist.txt"
    page_list = getPageList(website_file)
    homepage_template = Template(filename=template_file)
    rendered_homepage = homepage_template.render(websites=page_list)
    print(rendered_homepage)

